﻿using EventMgmt.Common.Models;
using Microsoft.WindowsAzure.Storage.Table;
using System.Threading.Tasks;

namespace EventMgmt.Common.Database
{
    public interface IRepository
    {
        Task InitDbAsync();
        Task<TableQuerySegment<Event>> GetEventsAsync(IEventDB eventDb);
        Task<Event> InsertOrMergeEntityAsync(Event entity);
        Task<Event> RetrieveEntityUsingPointQueryAsync(string partitionKey, string rowKey);
        Task DeleteEntityAsync(CloudTable table, Event deleteEntity);
    }
}
