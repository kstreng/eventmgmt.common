﻿using EventMgmt.Common.Helpers;
using EventMgmt.Common.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Threading.Tasks;

namespace EventMgmt.Common.Database
{
    public class Repository : IRepository
    {
        public async Task InitDbAsync()
        {
            CloudTable table = await TableStorage.CreateTableAsync(MyDomain.TABLENAME);

            await this.InitDbAsync(table);
        }

        public async Task InitDbAsync(CloudTable table)
        {
            for (int idx = 1; idx < 4; idx++)
            {
                Event @event = new Event($"Customer{idx}", $"Event {idx}");
                await this.InsertOrMergeEntityAsync(table, @event);
            }
        }

        public async Task<TableQuerySegment<Event>> GetEventsAsync(IEventDB eventDb)
        {
            CloudTable table = await TableStorage.CreateTableAsync(MyDomain.TABLENAME);

            TableQuery<Event> query = new TableQuery<Event>()
                    .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, eventDb.CurrentCustomer));
            var result = await table.ExecuteQuerySegmentedAsync<Event>(query, null);

            return result;
        }

        public async Task<Event> InsertOrMergeEntityAsync(Event entity)
        {
            CloudTable table = await TableStorage.CreateTableAsync(MyDomain.TABLENAME);
            await this.InsertOrMergeEntityAsync(table, entity);

            return entity;
        }

        private async Task<Event> InsertOrMergeEntityAsync(CloudTable table, Event entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            try
            {
                // Create the InsertOrReplace table operation
                TableOperation insertOrMergeOperation = TableOperation.InsertOrMerge(entity);

                // Execute the operation.
                TableResult result = await table.ExecuteAsync(insertOrMergeOperation);
                Event insertedEvent = result.Result as Event;

                //// Get the request units consumed by the current operation. RequestCharge of a TableResult is only applied to Azure Cosmos DB
                //if (result.RequestCharge.HasValue)
                //{
                //    Console.WriteLine("Request Charge of InsertOrMerge Operation: " + result.RequestCharge);
                //}

                return insertedEvent;
            }
            catch (StorageException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                throw;
            }
        }

        public async Task<Event> RetrieveEntityUsingPointQueryAsync(string partitionKey, string rowKey)
        {
            CloudTable table = await TableStorage.CreateTableAsync(MyDomain.TABLENAME);

            try
            {
                TableQuery<Event> query = new TableQuery<Event>()
                    .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey));
                var result2 = await table.ExecuteQuerySegmentedAsync<Event>(query, null);


                TableOperation retrieveOperation = TableOperation.Retrieve<Event>(partitionKey, rowKey);
                TableResult result = await table.ExecuteAsync(retrieveOperation);
                Event @event = result.Result as Event;
                if (@event != null)
                {
                    Console.WriteLine($"\t{@event.PartitionKey}\t{@event.RowKey}\t{@event.Name}");
                }

                //// Get the request units consumed by the current operation. RequestCharge of a TableResult is only applied to Azure CosmoS DB 
                //if (result.RequestCharge.HasValue)
                //{
                //    Console.WriteLine("Request Charge of Retrieve Operation: " + result.RequestCharge);
                //}

                return @event;
            }
            catch (StorageException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                throw;
            }
        }

        public async Task DeleteEntityAsync(CloudTable table, Event deleteEntity)
        {
            try
            {
                if (deleteEntity == null)
                {
                    throw new ArgumentNullException("deleteEntity");
                }

                TableOperation deleteOperation = TableOperation.Delete(deleteEntity);
                TableResult result = await table.ExecuteAsync(deleteOperation);

                // Get the request units consumed by the current operation. RequestCharge of a TableResult is only applied to Azure CosmoS DB 
                //if (result.RequestCharge.HasValue)
                //{
                //    Console.WriteLine("Request Charge of Delete Operation: " + result.RequestCharge);
                //}

            }
            catch (StorageException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                throw;
            }
        }
    }
}
