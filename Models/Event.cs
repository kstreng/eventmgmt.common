﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace EventMgmt.Common.Models
{
    public class Event : TableEntity
    {
        public Event()
        {
        }

        public Event(string customer, string eventName)
        {
            PartitionKey = customer;
            RowKey = Guid.NewGuid().ToString();

            Customer = customer;
            Name = eventName;
        }

        public string Customer { get; set; }
        public string Name { get; set; }
    }
}
