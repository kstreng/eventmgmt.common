﻿using EventMgmt.Common.Database;
using Microsoft.Azure.Documents.Client;
using System.Collections.Generic;

namespace EventMgmt.Common.Models
{
    public class EventDB : IEventDB
    {
        IRepository _repository;
        public EventDB(IRepository repository)
        {
            _repository = repository;
        }

        private DocumentClient client;

        public string CurrentCustomer { get; set; }

        public IEnumerable<Event> LoadEvents()
        {
            // ....

            return _repository.GetEventsAsync(this).Result;
        }

        public void SaveEvent(Event @event)
        {
            // ....

            Event result = _repository.InsertOrMergeEntityAsync(@event).Result;
        }
    }
}
