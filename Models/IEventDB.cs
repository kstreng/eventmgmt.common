﻿using System.Collections.Generic;

namespace EventMgmt.Common.Models
{
    public interface IEventDB
    {
        IEnumerable<Event> LoadEvents();
        void SaveEvent(Event @event);
        string CurrentCustomer { get; set; }
    }
}
